define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'finance/partner/index',

                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'uid',
                columns: [
                    [
                        {field: 'uid', title: '用户id'},
                        {field: 'user_name', title: __('用户昵称')},
                        {field: 'level', title: __('用户级别'),formatter:Controller.api.formatter.level,searchable:false},
                        {field: 'ys_income', title: __('原始股金额'),operate: false},
                        {field: 'jd_income', title: __('季度分红金额'),operate: false},
                        {field: 'cs_income', title: __('城市加权金额'),operate: false},
                        {field: 'create_time', title: __('加入时间'), formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        
                    ]
                ]
            });


            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        api: {
            formatter: {
                level: function (value, row, index) {
                    var title = '';
                    if(value == 2){
                        title = '城市合伙人';
                    }else if(value == 3){
                        title = '区县合伙人';
                    }else if(value == 4){
                        title = '乡镇合伙人';
                    }else{
                        title = '共创合伙人';
                    }
                    return title;
                },
            },

            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    
    return Controller;
});
