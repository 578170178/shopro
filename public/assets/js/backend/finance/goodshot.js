define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'finance/goodshot/index',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                sortName: 'date',
                searchFormVisible: true,
                searchFormTemplate: 'customformtpl',
                columns: [
                    [
                        {field: 'date', title: '日期', sortable: true},
                        {field: 'goods_id', title: '商品ID'},
                        {field: 'goods_title', title: '商品名称'},
                        {field: 'cnt', title: '订单数'},
                        {field: 'sum_num', title: '销售数量'},
                        {field: 'price_cnt', title: '销售额'},
                        {field: 'cost_cnt', title: '估算成本'},
                        {field: 'commission_cnt', title: '估算佣金'},
                        {field: 'profit', title: '估算利润'},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        api: {
            formatter: {
            },
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});