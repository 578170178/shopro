define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'retail/retailconf/index',
                    // add_url: 'retail/retailconf/add',
                    edit_url: 'retail/retailconf/edit',
                    // del_url: 'retail/retailconf/del',
                    // multi_url: 'retail/retailconf/multi',
                    // table: 'retail_config',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {field: 'id', title: __('Id'), sortable: true},
                        {field: 'level', title: __('用户级别'),formatter:Controller.api.formatter.level,searchable:false},
                        {field: 'recommend', title: __('推荐元')},
                        {field: 'under_wanjia', title: __('直属玩家佣金%')},
                        {field: 'team', title: __('团队销售额佣金%')},
                        {field: 'children', title: __('团队育成佣金%')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            formatter: {
                level: function (value, row, index) {
                    var title = '';
                    if(value == 2){
                        title = '玩客';
                    }else if(value == 3){
                        title = '玩主';
                    }else if(value == 4){
                        title = '玩家';
                    }else{
                        title = '粉丝';
                    }
                    return title;
                },
            },
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});