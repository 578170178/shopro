define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'retail/partner/index',
                    add_url: 'retail/partner/add',
                    edit_url: 'retail/partner/edit',
                    del_url: 'retail/partner/del',
                    multi_url: 'retail/partner/multi',
                }
            });

            var table = $("#table");

            // //在表格内容渲染完成后回调的事件
            // table.on('post-body.bs.table', function (e, json) {
            //     $("tbody tr[data-index]", this).each(function () {
            //         if (parseInt($("td:eq(1)", this).text()) == Config.admin.id) {
            //             $("input[type=checkbox]", this).prop("disabled", true);
            //         }
            //     });
            // });

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                columns: [
                    [
                        {field: 'uid', title: '用户id'},
                        {field: 'user_name', title: __('用户昵称')},
                        {field: 'level', title: __('用户级别'),formatter:Controller.api.formatter.level,searchable:false},
                        {field: 'di_qu', title: __('代理地区'),operate: false},
                        {field: 'create_time', title: __('加入时间'), formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: function (value, row, index) {
                               
                                return Table.api.formatter.operate.call(this, value, row, index);
                            }}
                    ]
                ]
            });

            $(".selectpage").on("change",function(e){
                console.log(2222);
            }),

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Form.api.bindevent($("form[role=form]"));
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
        },
        api: {
            formatter: {
                level: function (value, row, index) {
                    var title = '';
                    if(value == 2){
                        title = '城市合伙人';
                    }else if(value == 3){
                        title = '区县合伙人';
                    }else if(value == 4){
                        title = '乡镇合伙人';
                    }else{
                        title = '共创合伙人';
                    }
                    return title;
                },
            },

            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    
    return Controller;
});
