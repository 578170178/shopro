<?php

namespace app\admin\model\shopro\activity;

use think\Model;
use traits\model\SoftDelete;

class Bargain extends Model
{

    use SoftDelete;

    

    // 表名
    protected $name = 'shopro_activity_bargain';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';

    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    public static function getTypeList()
    {
        return ['gift' => '礼包', 'travel' => '旅游'];
    }
}
