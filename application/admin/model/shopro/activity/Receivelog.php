<?php

namespace app\admin\model\shopro\activity;

use think\Model;

class Receivelog extends Model
{


    // 表名
    protected $name = 'retail_userdraw_gift';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';


}
