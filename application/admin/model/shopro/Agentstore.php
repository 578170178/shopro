<?php

namespace app\admin\model\shopro;

use think\Model;
use traits\model\SoftDelete;

class Agentstore extends Model
{

    use SoftDelete;

    

    // 表名
    protected $name = 'retail_agent';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [

    ];


    /**
     * ID=>Name 的集合
     */
    public static function idAndNameLists()
    {
        return (new self())->column('id,name');
    }







}
