<?php

namespace app\admin\model\finance;

use think\Model;

class Retailorder extends Model
{


    // 表名
    protected $name = 'retail_order_commission';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'create_time';


}
