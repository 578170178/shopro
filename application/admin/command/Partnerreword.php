<?php

/**
 * User: Yzw
 * Date: 2020/12/10
 * 定时执行 统计发放合伙人的收益
 * 默认 0/1 0 0 * * 每日凌晨01分
 *
 * 每个月1号执行脚本,更新玩家拥有的权益
 */
namespace app\admin\command;

use app\common\model\RetailReward;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Exception;
use think\Db;

class Partnerreword extends Command
{
    protected function configure()
    {
        $this->setName('Partnerreword')->setDescription('统计发放每一天合伙人的收益');
    }
    protected function execute(Input $input, Output $output)
    {    
        // 获取到所有平台的订单纯收益
        $begin_time   = (time() - 8 *24 *3600);
        $end_time = (time() - 7 *24 *3600);

        $where = ['createtime'=>['between',"$begin_time,$end_time"],'dispatch_status'=>'2'];
        $allmoney = db('shopro_order_item')
                    ->field('SUM(goods_price) as money, SUM(cost_price) as cost, SUM(commission) as com')
                    ->where($where)
                    ->find();
        $money = $allmoney['money'] - $allmoney['cost'] - $allmoney['com']; /**净收益 */

        //1.获取到所有的合伙人
        $partner = db('retail_partner')->field('ys_conf,jd_conf,cs_conf,uid,zone_id')->select();
        $partner = \collection($partner)->toArray();
        foreach ($partner as $item){    
            $useridsarr[] = $item['uid'];
            
        }

        /**2获取到符合下级也成为合伙人的目标合伙人*/
        $platform_uid = db('retail_partner')
                        ->alias('p')
                        ->field('u.pid')
                        ->join('retail_user as u','p.uid=u.uid')
                        ->select();
        $platform_uid = \collection($platform_uid)->toArray();
        foreach ($platform_uid as $item){    
            $platuserid[] = $item['pid'];

        }
        $itemuser = array_intersect($useridsarr,$platuserid);

        try{
            Db::startTrans();
            try{
                foreach ($partner as $item){  
                    $ys_income = $money * $item['ys_conf']; /**原始收益 */    
                    foreach ($itemuser as $k => $val) {
                        if($item['uid'] == $val){
                            $jd_income = $money * $item['jd_conf']; /**季度收益 */
                        }else{
                            $jd_income = '0.00';
                        }
                    }
    
                    /**获取到所有合伙人下城市内的用户 */
                    $dbuser = db('retail_user_address')->field('user_id')->where('province_id or city_id or area_id or zone_id',$item['zone_id'])->select();
                    $zoneres = \collection($dbuser)->toArray();
                    foreach ($zoneres as $key) {
                        $zone_user[] = $key['user_id'];
                    }
                    $userids = implode(",",$zone_user);
                    $csmoney = db('shopro_order_item')
                            ->field('SUM(goods_price) as money, SUM(cost_price) as cost, SUM(commission) as com')
                            ->where($where)
                            ->where('uid','in',$userids)
                            ->find();
                    $csreword = $csmoney['money'] - $csmoney['cost'] - $csmoney['com']; /**城市净收益 */
                    $cs_income = $csreword * $item['cs_conf']; /**城市个人收益 */
    
                    unset($zone_user);unset($dbuser); unset($csmoney);
                    /**插入每日收益的日志 */
                    $reword_log = [
                        'uid'      => $item['uid'],
                        'ys_income'=> $ys_income,
                        'jd_income'=> $jd_income,
                        'cs_income'=> $cs_income,
                        'create_time' => time(),
                    ];
                    $insertres = db('retail_partner_reword_log')->insert($reword_log);
                    unset($reword_log);
                }
                Db::commit();
            }catch (\Exception $e){
                Db::rollback();
                \think\log::error("更新每日利润:".$e->getMessage());
                exit;
            }

        }catch (\Exception $e){
            \think\log::error("更新每日利润:".$e->getMessage());
            exit;
        }

        echo "更新成功";exit;
    }
}