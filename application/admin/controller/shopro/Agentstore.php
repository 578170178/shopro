<?php

namespace app\admin\controller\shopro;

use app\common\controller\Backend;
use think\Db;

/**
 * 供销商
 *
 * @icon fa fa-circle-o
 */
class Agentstore extends Backend
{
    
    /**
     * Strategy模型对象
     * @var \app\admin\model\shopro\Agentstore
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\shopro\Agentstore;

    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

     /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post();
            if ($params) {
                $params = $params['row'];
                $params = $this->doarea($params);
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->model->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }


        return $this->view->fetch();
    }

    /**
     * 处理地址信息
     */
    public function doarea(&$params){

        if(!isset($params['city_id']) || !isset($params['area_id']) || !isset($params['area_id'])){
            $this->error('地址信息不能为空');   
        }
        $zone_id = $params['area_id'];
        $area_name = db('cnarea2019')->field('merger_name')->where('area_code',$zone_id)->find();
        $params['area_name'] = $area_name['merger_name'].'，'.$params['address'];
        return $params;

    }

    /**
     * 供销商信息
     */
    public function select()
    {
        $ids = $this->request->get();
        $agentData = $this->model->order('id asc')->select();
        $this->view->assign('agentData', $agentData);
        return $this->view->fetch();
    }

    /**
     * 获取发货类型
     */
    public function agentList()
    {
        $typeList = \app\admin\model\shopro\Agentstore::idAndNameLists();

        $this->success('获取成功', null, $typeList);
    }



}
