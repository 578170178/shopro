<?php

namespace app\admin\controller\shopro\activity;

use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

use addons\shopro\library\traits\ActivityCache;

/**
 * 小礼包配置
 *
 * @icon fa fa-circle-o
 */
class Giftgive extends Backend
{
    use ActivityCache;
    /**
     * Activity模型对象
     * @var \app\admin\model\shopro\activity\Giftgive
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\shopro\activity\Giftgive;
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.phpgive中对应的方法复制到当前控制器,然后进行修改
     */


    /**
     * 查看活动列表
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            // list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $sort = $this->request->get("sort", !empty($this->model) && $this->model->getPk() ? $this->model->getPk() : 'id');
            $order = $this->request->get("order", "DESC");
            $offset = $this->request->get("offset", 0);
            $limit = $this->request->get("limit", 0);
            $total = $this->buildSearch()
                ->order($sort, $order)
                ->count();

            $list = $this->buildSearch()
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();
            $list = collection($list)->toArray();

            $result = array("total" => $total, "rows" => $list);
            if ($this->request->get("page_type") == 'select') {
                return json($result);
            }
            return $this->success('操作成功', null, $result);
        }
        return $this->view->fetch();
    }


    public function all() {
        if ($this->request->isAjax()) {
            $type = $this->request->get('type', 'all');

            $sort = $this->request->get("sort", !empty($this->model) && $this->model->getPk() ? $this->model->getPk() : 'id');
            $order = $this->request->get("order", "DESC");

            $activities = $this->model->withTrashed();               // 包含被删除的
            if ($type != 'all') {
                $activities = $activities->where('type', $type);
            }

            $activities = $activities
                ->field('id, title, type, endtime, rules')
                ->order($sort, $order)
                ->select();

            $activities = collection($activities)->toArray();

            return $this->success('操作成功', null, $activities);
        }
    }


    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post();
            if ($params) {
                if ($params['starttime'] > $params['endtime'] || $params['endtime'] < date('Y-m-d H:i:s')) {
                    $this->error('请设置正确的活动时间');
                }
                $params = $this->preExcludeFields($params);

                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $result = false;
                Db::startTrans();
                try {
                    if (empty($params['type'])) {
                        $this->error('请选择礼包类型');
                    }
                    $result = $this->model->allowField(true)->save($params);

                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }

        return $this->view->fetch();
    }


    /**
     * 高级礼包赠送礼包添加
     */
    public function addgive($ids)
    {   
        if ($this->request->isPost()) {
            $params = $this->request->post();
            if ($params) {
                $params = $this->preExcludeFields($params);
                $gift_id = $this->request->get();
                $params['gift_id'] = $gift_id['ids'];
                $result = false;
                Db::startTrans();
                try {
                    $giveModel = new \app\admin\model\shopro\activity\Giftgive;
                    $result = $giveModel->allowField(true)->save($params);

                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }


    /**
     * 获取详情
     */
    public function detail ($ids = null) {
        //详情
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }

        $goods_ids_array = explode(',', $row->goods_id);
        $goodsList = [];
        foreach ($goods_ids_array as $k => $g) {
            $goods[$k] = \app\admin\model\shopro\goods\Goods::field('id,title,image')->where('id', $g)->find();
            $goods[$k]['actSkuPrice'] = json_encode(\app\admin\model\shopro\activity\ActivitySkuPrice::all(['goods_id' => $g, 'activity_id' => $ids]));

            $goods[$k]['opt'] = 1;
            $goodsList[] = $goods[$k];
        }

        $row->goods_list = $goodsList;
        return $this->success('获取成功', null, $row);
    }


    
    /**
     * 获取赠送配置详情
     */
    public function detailgive ($ids = null) {
        //详情
        $gift_id = $this->request->get();
        $type = $gift_id['ids'];
        if (!$gift_id) {
            $this->error(__('No Results were found'));
        }
        $giveModel = new \app\admin\model\shopro\activity\Giftgive;
        $where['type'] = $type;
        $list = $giveModel->where($where)->order('id desc')->select();

        $list = collection($list)->toArray();
        return $this->success('获取成功', null, $list);
    }


    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        //编辑
        $row = $this->model->get($ids);
        $id = $row['id'];
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {

            $params = $this->request->post();
            if ($params) {
                $last_info = db('retail_gift_give')->where('id',$id)->find();

                $params = $this->preExcludeFields($params);
                if($params['salestatus'] == 2 && $last_info['salestatus'] == 1){
                    $this->error(__('该路线正在报名ing不允许改动为即将开启')); 
                }

                if($params['salestatus'] == 2){
                    db('retail_gift_give')->where('id',$id)->update(['is_send_msg'=>'0']);
                }
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }

                    $result = $row->allowField(true)->save($params);

                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->assignconfig("activity", $row);
        $this->view->assign("row", $row);
        $this->assignconfig('id', $ids);
        
        return $this->view->fetch();
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds)) {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($pk, 'in', $ids)->select();

            $count = 0;
            Db::startTrans();
            try {
                foreach ($list as $k => $v) {
                    $count += $v->delete();
                }
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }



    // 构建查询条件
    private function buildSearch()
    {
        $search = $this->request->get("search", '');        // 关键字
        $type = $this->request->get("type", 'all');
        $status = $this->request->get("status", 'all');

        $name = $this->model->getQuery()->getTable();
        $tableName = $name . '.';

        $activities = $this->model;
        if ($search) {
            // 模糊搜索字段
            $searcharr = ['title'];
            foreach ($searcharr as $k => &$v) {
                $v = stripos($v, ".") === false ? $tableName . $v : $v;
            }
            unset($v);
            $activities = $activities->where(function ($query) use ($searcharr, $search, $tableName) {
                $query->where(implode("|", $searcharr), "LIKE", "%{$search}%");
            });
        }
        // 活动类型
        if ($type != 'all') {
            $activities = $activities->where('type', 'like', '%' . $type . '%');
        }
        // 礼包状态
        if ($status != 'all') {
            $where = [];
            if ($status == 'up') {
                $where['status'] = ['=', 'up'];
            } else if ($status == 'hidden') {
                $where['status'] = ['=', 'hidden'];
            } else if ($status == 'down') {
                $where['status'] = ['=', 'down'];
            }

            $activities = $activities->where($where);
        }
        return $activities;
    }

}
