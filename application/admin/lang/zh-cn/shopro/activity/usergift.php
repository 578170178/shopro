<?php

return [
    'Id'             => 'ID',
    'User_id'        => '用户ID',
    'Phone_num'      => '手机号',
    'Gift_if'        => '礼包id',
    'Live_time'      => '有效时长',
    'User_name'      => '用户姓名',
    'Id_card'        => '身份证号码',
    'Createtime'     => '创建时间',
    'Gift'           => '高级礼包',
    'Travel'         => '旅游套餐',
];
