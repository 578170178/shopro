<?php

namespace app\common\model;

use think\Cache;
use think\Model;

/**
 * 奖池奖金数据模型
 */
class RetailReward extends Model
{
    const STATUS_ENABLE     = 1;//正常
    const STATUS_DISABLE    = 2;//分配完成
}
