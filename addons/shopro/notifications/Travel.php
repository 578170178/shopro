<?php

namespace addons\shopro\notifications;

use think\queue\ShouldQueue;

/**
 * 旅游活动通知
 */
class Travel extends Notification implements ShouldQueue
{
    // 队列延迟时间，必须继承 ShouldQueue 接口
    public $delay = 0;

    // 发送类型 复合型消息类动态传值 当前类支持的发送类型: groupon_success: 拼团成功, groupon_fail: 拼团失败
    public $event = '';

    // 额外数据
    public $data = [];

    // 返回的字段列表
    public static $returnField = [
        'travel_start' => [
            'name' => '旅游路线即将开启通知',
            'fields' => [
                ['name' => '旅游路线ID', 'field' => 'travel_id'],
                ['name' => '旅游路线名称', 'field' => 'title'],
                ['name' => '活动时间', 'field' => 'start_time'],
                ['name' => '备注', 'field' => 'remark'],
            ]
        ]
    ];


    public function __construct($data = [])
    {
        $this->data = $data;
        $this->event = $data['event'] ?? '';

        $this->initConfig();
    }


    public function toDatabase($notifiable) {
        $data = $this->data;

        // 获取消息data
        $this->paramsData($params, $notifiable);

        return $params;
    }


    public function toSms($notifiable) {
        $event = $this->event;
        $data = $this->data;
        $groupon = $data['groupon'];
        $grouponLogs = $data['grouponLogs'];
        $grouponLeader = $data['grouponLeader'];
        $goods = $data['goods'];

        $phone = $notifiable['mobile'] ? $notifiable['mobile'] : '';
        $params = [];
        $params['phone'] = $phone;

        // 获取消息data
        $this->paramsData($params, $notifiable, $groupon, $grouponLogs, $grouponLeader, $goods);

        return $this->formatParams($params, 'sms');
    }


    public function toEmail($notifiable)
    {
        $event = $this->event;
        $data = $this->data;
        $groupon = $data['groupon'];
        $grouponLogs = $data['grouponLogs'];
        $grouponLeader = $data['grouponLeader'];
        $goods = $data['goods'];

        $params = [];

        // 获取消息data
        $this->paramsData($params, $notifiable, $groupon, $grouponLogs, $grouponLeader, $goods);

        return $this->formatParams($params, 'email');
    }


    public function toWxOfficeAccount($notifiable) {
        $event = $this->event;
        $data = $this->data;
        $groupon = $data['groupon'];
        $grouponLogs = $data['grouponLogs'];
        $grouponLeader = $data['grouponLeader'];
        $goods = $data['goods'];

        $params = [];

        if ($oauth = $this->getWxOauth($notifiable, 'wxOfficialAccount')) {
            // 拼团详情
            $path = "/pages/activity/groupon/detail?id=" . $groupon['id'];

            // 获取 h5 域名
            $url = $this->getH5DomainUrl($path);

            $params['openid'] = $oauth->openid;
            $params['url'] = $url;

            // 获取消息data
            $this->paramsData($params, $notifiable, $groupon, $grouponLogs, $grouponLeader, $goods);
        }

        return $this->formatParams($params, 'wxOfficialAccount');
    }


    public function toWxMiniProgram($notifiable) {
        $event = $this->event;
        $data = $this->data;
        $travel_info = $this->data['travel_info'];

        $params = [];

        if ($oauth = $this->getWxOauth($notifiable, 'wxMiniProgram')) {
            // 旅游小礼包详情
            $path = "/pages/activity/Countrytour/Country";

            // 获取小程序完整路径
            $path = $this->getMiniDomainUrl($path);

            $params['openid'] = $oauth->openid;
            $params['page'] = $path;

            // 获取消息data
            $this->paramsData($params, $notifiable);
        }

        return $this->formatParams($params, 'wxMiniProgram');
    }


    private function paramsData(&$params, $notifiable) {
        $params['data'] = [];
        switch ($this->event) {
            case 'travel_start':
                $params['data'] = $this->data['travel_info'];
                break;
            default:
                $params = [];
        }
    }




}
