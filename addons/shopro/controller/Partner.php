<?php

namespace addons\shopro\controller;

use app\admin\model\shopro\activity\Gift;

/**
 * 合伙人
 *
 */

class Partner extends Base
{
    protected $noNeedLogin = ['index','applepartner','mypartner'];
    protected $noNeedRight = ['*'];

    protected $model = null;


    /**
     * 玩家申请成为合伙人记录
     */
    public function applepartner(){

        $auth = \app\common\library\Auth::instance();
        $auth->setAllowFields(['id', 'username', 'nickname']);
        $data = $auth->getUserinfo();
        $id   = $data['id'];
        $user_name = $this->request->post('user_name');
        if(!isset($user_name)){
            $this->error('参数缺失');
        }
        $info = [
            'uid'   =>$id,
            'nick_name' =>$data['nickname'],
            'user_name' =>$this->request->post('user_name'),
            'phone_num' =>$this->request->post('phone_num'),
            'create_time' =>time(),
        ];
        db('retail_partner_log')->insert($info);
        $this->success('信息填写成功','1');
    }


    /**
     * 我的合伙人
     */
    public function mypartner(){

        $auth = \app\common\library\Auth::instance();
        $auth->setAllowFields(['id', 'username', 'nickname']);
        $data = $auth->getUserinfo();
        $id   = $data['id'];

        $dbuser = db('retail_user')->field('uid,level')->where('uid',$data['id'])->find();
        
        $teamids[]  = $dbuser['uid'];
        while ($dbuser['level'] > 1){
            //找到直属下级
            $dbuser['level']--;

            $children = db('retail_user')->where(['level'=>$dbuser['level'],'pid'=>['in',$teamids]])->column('uid');
            $teamids = array_merge($teamids,$children);
        }

        $info = db('retail_partner')
            ->alias("a")
            ->field('a.tj_income,a.uid,i.level,p.mobile,p.nickname,p.avatar,a.create_time')
            ->join('retail_user i', 'a.uid = i.uid')
            ->join('user p', 'a.uid = p.id')
            ->where(['a.uid'=>['in',$teamids]])
            ->order('a.create_time','desc')
            ->select();

        $info = collection($info)->toArray();
        $this->success('我的合伙人列表',$info);
    }

}
