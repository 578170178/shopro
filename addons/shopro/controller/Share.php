<?php

namespace addons\shopro\controller;

use addons\shopro\model\Share as ShareModel;
use app\common\model\RetailUser;
use think\Db;


class Share extends Base
{

    protected $noNeedLogin = [];
    protected $noNeedRight = ['*'];

    public function add()
    {
        $params = $this->request->post();
        $params['platform'] = $this->request->header('platform');

        $share_id= $this->request->post('share_id');
        if(!empty($share_id)){
           $this->uptofens($share_id);
        }

        $this->success('添加分享记录', ShareModel::add($params));
    }

    /**
     * 普通会员变成粉丝的接口(扫码绑定)
     * @param string share_id 用户绑定的上级ID
     */
    protected function uptofens($share_id)
    {

        $user = $this->auth->getUser();
        $user_id    = $user['id'];

        if(empty($user_id) || $user_id == $share_id){
            return 0;
        }
        //判断上级share_id是否是玩客或以上,不是则忽略
        $parent = db('retail_user')->find($share_id);
        if(empty($parent)){
            return 0;
            //$this->error('非法上级会员');
        }

        //判断user_id是否是普通用户,如果已经是会员了,即粉丝以上,则忽略
        $retail_user = db('retail_user')->find($user_id);
        if(!empty($retail_user)){
            return 0;
            $this->error('此用户已经绑定过上级会员了');
        }

        $total_types = array_merge(\addons\shopro\model\UserWalletLog::$payType,\addons\shopro\model\UserWalletLog::$retailType);
        $total_income = \addons\shopro\model\UserWalletLog::getSumByType('money',$total_types, 'all');

        /*******************开始成为粉丝*************/
        Db::startTrans();
        try{
            $retail_user_data = [
                'uid' => $user_id,
                'level' => RetailUser::LEVEL_FENS,
                'level_time'=> time(),
                'pid'   => $share_id,
                'total_income' => $total_income,
                'create_time'=> time(),
            ];
            //retail_user添加一条数据
            db('retail_user')->insert($retail_user_data);

            //更新上级的直属粉丝数量和累计粉丝数量
            $updata = [
                'fens_num' => $parent['fens_num'] + 1,
                'fens_total' => $parent['fens_total'] + 1,
            ];

            db('retail_user')->where(['uid'=>$parent['uid']])->update($updata);

            Db::commit();

        }catch (\Exception $e){
            Db::rollback();
            return 0;
            //$this->error($e->getMessage());
        }

        /***************循环添加累计粉丝**************/
        $child = $parent;

        $count = 10000;//最多有10000级
        while( !empty($child) && $count !=0 ){
            $count--;

            //上级会员
            $parent = db('retail_user')->where(['uid'=>$child['pid']])->find();
            if(empty($parent) || $parent['uid'] == $child['uid']){
                break;
            }

            $updata = [];//更新的数据
            $updata['fens_total']   = $parent['fens_total'] + 1;//累计

            //更新上级数据
            db('retail_user')->where(['uid'=>$parent['uid']])->update($updata);

            //继续获取下一个上级信息
            $child  = $parent;
        }


        return 1;
    }


}