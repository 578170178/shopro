<?php

namespace addons\shopro\listener\order;

use addons\shopro\exception\Exception;
use addons\shopro\model\ActivityGoodsSkuPrice;
use addons\shopro\model\Config;
use addons\shopro\model\Goods;
use addons\shopro\model\GoodsSkuPrice;
use addons\shopro\model\User;
use addons\shopro\model\UserCoupons;
use addons\shopro\model\UserWalletLog;
use think\Db;

/**
 * 订单退款
 */
class Refund
{
    // 订单同意退款前
    public function orderRefundBefore(&$params)
    {
        $order = $params['order'];
        $item = $params['item'];

    }


    // 订单同意退款后
    public function orderRefundAfter(&$params)
    {
        $order = $params['order'];
        $item = $params['item'];

        /**退款后退回佣金操作 */
        $has = db('retail_order_commission')->where('order_id',$order['id'])->find();
        if($has){
            $this->refundcommission($has);
        }

        /**退款后退回团队佣金操作 */
        $hasteam = db('retail_team_commission')->where('order_id',$order['id'])->find();
        if($hasteam){
            $this->refundteamcommission($hasteam);
        }

        /**玩家退款乡村游小礼包操作 */
        if($order['activity_type'] == 'ltravel'){
            $this->refunduserbuy($order,$item['goods_id']);
        }

        // 订单退款成功
        $user = User::where('id', $order['user_id'])->find();
        $user->notify(
            new \addons\shopro\notifications\Refund([
                'order' => $order,
                'item' => $item,
                'event' => 'refund_agree'
            ])
        );
    }

    /**退回玩家礼包权益 */
    protected function refunduserbuy($order,$goods_id){
        /**更新玩家权益 */
        db('retail_userbuy_gift')->where(['user_id'=>$order['user_id']])->where(['type'=>'travel'])->setInc('receive_num',1);
        $gift_info =  db('retail_gift_give')->where(['goods_id'=>$goods_id])->where(['type'=>'travel'])->find();
        /**得到礼包id */
        db('retail_userdraw_gift')
            ->where(['gift_give_id'=>$gift_info['id'],'user_id'=>$order['user_id']])
            ->where(['type'=>'travel'])
            ->update(['status'=>'0']);
    }

    /**退款佣金 */
    protected function refundcommission($has){
        $WalletModel = new UserWalletLog();
        $userModel = new User();
        $share_user_id = $has['share_user_id'];
        $commission    = $has['commission'];

        $user = clone $userModel;
        $user = $user->find($has['share_user_id']);

        /**扣除玩家表的佣金 */
        db('user')->where(['id'=>$share_user_id])->setDec('money',$commission);
        db('retail_user')->where(['uid'=>$share_user_id])->setDec('total_income',$commission);
        db('retail_order_commission')->where(['share_user_id'=>$share_user_id,'order_id'=>$has['order_id']])->update(['is_back'=>'1']);
        /**钱包日志 */
        $userWallet = clone $WalletModel;
        $userWallet->doAdd($user, $commission, "sale_reward_refund", $has['id'], "money", 0);
        if(!empty($has['parent_id']) && !empty($has['parent_commission']) && $has['parent_commission'] !='0.00'){

            // if() /**如果玩家钱包少于需要扣除的佣金待处理 */

            /**有上上级佣金也需要扣除 */
            db('user')->where(['id'=>$has['parent_id']])->setDec('money',$has['parent_commission']);
            db('retail_user')->where(['uid'=>$has['parent_id']])->setDec('total_income',$has['parent_commission']);

            $userParent = clone $userModel;
            $parent = $userParent->find($has['parent_id']);

            $parentWallet = clone $WalletModel;
            $parentWallet->doAdd($parent, $has['parent_commission'], "sale_reward_zhishu_refund", $has['id'], "money", 0);
        }

    }


    /**退款团队佣金 */
    protected function refundteamcommission($hasteam){
        $WalletModel = new UserWalletLog();
        $userModel = new User();
        $user_id   = $hasteam['user_id'];
        $commission    = $hasteam['commission'];

        $user = clone $userModel;
        $user = $user->find($hasteam['user_id']);

        /**扣除玩家表的佣金 */
        db('user')->where(['id'=>$user_id])->setDec('money',$commission);
        db('retail_user')->where(['uid'=>$user_id])->setDec('total_income',$commission);
        db('retail_team_commission')->where(['user_id'=>$user_id,'order_id'=>$hasteam['order_id']])->update(['is_back'=>'1']);
        /**钱包日志 */
        $userWallet = clone $WalletModel;
        $userWallet->doAdd($user, $commission, "sale_reward_team_refund", $hasteam['id'], "money", 0);
        if(!empty($hasteam['parent_id']) && !empty($hasteam['parent_commission']) && $hasteam['parent_commission'] !='0.00'){

            // if() /**如果玩家钱包少于需要扣除的佣金待处理 */

            /**有上上级佣金也需要扣除 */
            db('user')->where(['id'=>$hasteam['parent_id']])->setDec('money',$hasteam['parent_commission']);
            db('retail_user')->where(['uid'=>$hasteam['parent_id']])->setDec('total_income',$hasteam['parent_commission']);

            $userParent = clone $userModel;
            $parent = $userParent->find($hasteam['parent_id']);

            $parentWallet = clone $WalletModel;
            $parentWallet->doAdd($parent, $hasteam['parent_commission'], "sale_reward_team_refund", $hasteam['id'], "money", 0);
        }
    }

    // 订单退款拒绝前（钩子即将废弃 2020-07-28）
    public function orderRefundRefuseBefore(&$params)
    {
        $order = $params['order'];
        $item = $params['item'];
    }


    // 订单退款拒绝后（钩子即将废弃 2020-07-28）
    public function orderRefundRefuseAfter(&$params)
    {
        $order = $params['order'];
        $item = $params['item'];

        // 退款拒绝
        $user = User::where('id', $order['user_id'])->find();
        $user->notify(
            new \addons\shopro\notifications\Refund([
                'order' => $order,
                'item' => $item,
                'event' => 'refund_refuse'
            ])
        );
    }
}
