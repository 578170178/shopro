<?php

namespace addons\shopro\model;

use addons\shopro\exception\Exception;
use think\Model;
use think\Db;

/**
 * 礼包日志
 */
class Giftlog extends Model
{
    // 表名,不含前缀
    protected $name = 'retail_userbuy_giftlog';
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';

    protected $hidden = [];


    //写入日志
    public static function insertGiftlog($row){
        if(!empty($row)){
            $money = $row['total_fee'];
            unset($row['total_fee']);
            $info = db('retail_userbuy_giftlog')->insert($row);
            /**获取玩家权益信息 */
            $userinfo = db('retail_userbuy_gift')->where('user_id',$row['user_id'])->where('type',$row['type'])->find();

            /**判断是否为大礼包 */
            if($row['is_big'] ==1){
                if($info){
                    $giftinfo = db('retail_gift')->field('live_time,live_count')->where('id',$row['gift_id'])->find();
                    //判断玩家是否有这个礼包的权益 有更新权益权利
                    if($userinfo){
                        if($giftinfo['live_count'] != 0){
                            $resinfo = [
                                'buy_time'    => time() + ($giftinfo['live_time'] *24 *3600),
                                'receive_num' => $giftinfo['live_count'],
                                'gift_id' => $row['gift_id']
                            ];
                            db('retail_userbuy_gift')->where('user_id',$row['user_id'])->where('type',$row['type'])->update($resinfo);
                        }
                    //如果没有权益 插入操作
                    }else{
                        if($giftinfo['live_count'] != 0){
                            $resinfo = [
                                'type'    => $row['type'],
                                'user_id' => $row['user_id'],
                                'gift_id' => $row['gift_id'],
                                'buy_time'    => time() + ($giftinfo['live_time'] *24 *3600),
                                'receive_num' => $giftinfo['live_count'],
                                'createtime'  => time(),
                            ];
                            db('retail_userbuy_gift')->insert($resinfo);
                        }
                    }
                }
                return true;
            }else{
                /** 如果免费领取要走订单的小礼包记得写免费领取记录 */
                if($money == '0'){
                    $rewinfo = [
                        'type' => $row['type'],
                        'gift_give_id' => $row['gift_id'],
                        'user_id'=>$row['user_id'],
                    ];
                    if($row['type'] == 'gift'){
                        self::insReceivelog($rewinfo); //旅游小礼包报名时候已经有写入
                    }
                    /**更新一下用户权益里面可领取的当月次数 */
                    $resinfo = [
                        'receive_num' => $userinfo['receive_num']-1,
                    ];
                    db('retail_userbuy_gift')->where('user_id',$row['user_id'])->where('type',$row['type'])->update($resinfo);

                }
                return true;
            }
        }else{
            return false;
        }
    }

    /**
     * 统一写入领取免费日志
     */
    public static function insReceivelog($row){
        $row['createtime'] = time();
        $res = db('retail_userdraw_gift')->insert($row);
        if($res){
            return true;
        }
        return false;
    }

}
