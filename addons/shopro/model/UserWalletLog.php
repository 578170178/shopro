<?php

namespace addons\shopro\model;

use think\Model;
use addons\shopro\exception\Exception;
use think\Db;
use app\admin\library\Auth as AdminAuth;

/**
 * 钱包
 */
class UserWalletLog extends Model
{

    // 表名,不含前缀
    protected $name = 'shopro_user_wallet_log';
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    protected $hidden = ['deletetime'];


    // 追加属性
    protected $append = [
        'type_name',
        'wallet_type_name'
    ];

    public static $typeAll = [
        // money充值明细和提现付款
        'wallet_pay' => ['code' => 'wallet_pay', 'name' => '余额付款'],
        'recharge' => ['code' => 'recharge', 'name' => '用户充值'],
        'admin_recharge' => ['code' => 'admin_recharge', 'name' => '后台充值'],
        'admin_deduct' => ['code' => 'admin_deduct', 'name' => '后台扣除'],
        'cash' => ['code' => 'cash', 'name' => '提现'],
        'cash_error' => ['code' => 'cash_error', 'name' => '提现驳回'],
        'wallet_refund' => ['code' => 'wallet_refund', 'name' => '余额退款'],

        //分销收益
        'recommend' => ['code' => 'recommend', 'name' => '分享礼包贡献'],
        'reward_avg' => ['code' => 'reward_avg', 'name' => '奖池分配'],
        'sale_reward' => ['code' => 'sale_reward', 'name' => '产品分销奖励'],
        'sale_reward_zhishu' => ['code' => 'sale_reward_zhishu', 'name' => '直属销售贡献'],
        'sale_reward_refund' => ['code' => 'sale_reward_refund', 'name' => '分销产品退款'],
        'sale_reward_zhishu_refund' => ['code' => 'sale_reward_zhishu_refund', 'name' => '直属销售贡献(退款)'],
        'sale_reward_team'  => ['code' => 'sale_reward_team', 'name' => '团队销售贡献'],
        'sale_reward_team_refund'  => ['code' => 'sale_reward_team_fefund', 'name' => '团队销售贡献(退款)'],
        'sale_reward_yuchen' => ['code' => 'sale_reward_yuchen', 'name' => '团队育成佣金'],
        'travel_reward' =>  ['code' => 'travel_reward', 'name' => '旅游推荐奖励'],
        'groupon_back' =>  ['code' => 'groupon_back', 'name' => '抢购补贴金额'],
        'zhulikan_back' =>  ['code' => 'zhulikan_back', 'name' => '助力砍补贴金'],

        // partner 
        'partner_reward1' =>  ['code' => 'partner_reward1', 'name' => '拓展共创合伙人奖'],
        'partner_reward2' =>  ['code' => 'partner_reward2', 'name' => '拓展城市合伙人奖'],
        'partner_reward3' =>  ['code' => 'partner_reward3', 'name' => '拓展区县合伙人奖'],
        'partner_reward4' =>  ['code' => 'partner_reward4', 'name' => '拓展乡镇合伙人奖'],
        'partner_reward_pfadmin' =>  ['code' => 'partner_reward_pfadmin', 'name' => '平台分红收益'],
        'partner_reward_cs' =>  ['code' => 'partner_reward_cs', 'name' => '城市分红收益'],

        // score
        'sign' => ['code' => 'sign', 'name' => '签到'],
        'score_pay' => ['code' => 'score_pay', 'name' => '积分付款'],
        'score_back_order' => ['code' => 'score_back_order', 'name' => '取消订单退回'],
        'partner_admin_reword1'=>['code' => 'partner_admin_reword1','name'=>'帮扶拓展共创积分'],
        'partner_admin_reword2'=>['code' => 'partner_admin_reword2','name'=>'帮扶拓展城市积分'],
        'partner_admin_reword3'=>['code' => 'partner_admin_reword3','name'=>'帮扶拓展区县积分'],
        'partner_admin_reword4'=>['code' => 'partner_admin_reword4','name'=>'帮扶拓展乡镇积分'],
    ];

    //充值明细+提现付款
    public static $rechargeType = ['recharge','admin_recharge','admin_deduct','wallet_pay','cash','cash_error','wallet_refund'];
    //抢购和助力砍收益
    public static $payType = ['groupon_back','zhulikan_back'];
    //分销收益
    public static $retailType = ['recommend','reward_avg','sale_reward','sale_reward_zhishu','sale_reward_team','sale_reward_yuchen','travel_reward','sale_reward_refund','sale_reward_zhishu_refund','sale_reward_team_refund'];
    //合伙人收益
    public static $partnerType = ['partner_reward1','partner_reward2','partner_reward3','partner_reward4','partner_reward_pfadmin','partner_reward_cs'];

    public static $walletTypeAll = [
        'money' => '余额',
        'score' => '积分'
    ];

    public function scopeMoney($query)
    {
        return $query->where('wallet_type', 'money');
    }

    public function scopeScore($query)
    {
        return $query->where('wallet_type', 'score');
    }

    public function scopeAdd($query)
    {
        return $query->where('wallet', '>', 0);
    }

    public function scopeReduce($query)
    {
        return $query->where('wallet', '<', 0);
    }


    public static function doAdd($user, $wallet, $type, $item_id, $wallet_type, $is_add = 0, $ext = [])
    {
        // $self = new self();

        // $self->user_id = $user->id;
        // $self->wallet = $wallet;
        // $self->type = $type;                     // 这个字段受到  model type 影响
        // $self->item_id = $item_id;
        // $self->wallet_type = $wallet_type;
        // $self->is_add = $is_add;
        // $self->ext = json_encode($ext);
        // $self->save();

        // 自动获取操作人
        if (strpos(request()->url(), 'store.store') !== false) {
            // 门店
            $oper = Store::info();
            $oper_type = 'store';
            $oper_id = $oper ? $oper['id'] : 0;
        } else if (strpos(request()->url(), 'addons') !== false) {
            // 用户
            $oper = User::info();
            $oper_type = 'user';
            $oper_id = $oper ? $oper->id : $user['id'];
        } else {
            $adminAuth = AdminAuth::instance();     // 没有登录返回的还是这个类实例
            $oper = null;
            if ($adminAuth){
                $oper = $adminAuth->getUserInfo();
            }
            if ($oper) {
                $oper_type = 'admin';
                $oper_id = $oper['id'];
            } else {
                $oper_type = 'system';
                $oper_id = 0;
            }
        }

        $self = self::create([
            "user_id" => $user->id,
            "wallet" => $is_add ? $wallet : -$wallet,       // 符号直接存到记录里面
            "type" => $type,
            "item_id" => $item_id,
            "wallet_type" => $wallet_type,
            // "is_add" => $is_add,
            "ext" => json_encode($ext),
            "oper_type" => $oper_type,
            "oper_id" => $oper_id
        ]);

        // 钱包变动通知
        $user->notify(
            new \addons\shopro\notifications\Wallet([
                'walletLog' => $self,
                'event' => $wallet_type == 'money' ? 'wallet_change' : 'score_change'
            ])
        );

        return $self;
    }


    public static function getList($wallet_type, $status = 'all')
    {
        $user = User::info();

        $walletLogs = self::{$wallet_type}();

        if ($status != 'all') {
            $walletLogs = $walletLogs->{$status}();
        }

        $walletLogs = $walletLogs->where(['user_id' => $user->id])
            ->order('id', 'DESC')->paginate(10);
        foreach ($walletLogs as $w) {
            $w->avatar = $user->avatar;

            switch ($w['type']) {
                case 'wallet_pay':
                case 'wallet_refund':
                    $item = OrderItem::get($w->item_id);
                    $w->avatar = $item['goods_image'] ?? '';
                    $w->title = $item['goods_title'] ?? '';
                    break;
                case 'cash':
                case 'cash_error':
                    $userWalletApply = UserWalletApply::get($w->item_id);
                    $apply = ($userWalletApply && $userWalletApply['bank_info']) ? json_decode($userWalletApply['bank_info'], true) : [];
                    $w->avatar = $user->avatar;
                    $w->title = $apply['bank_name'] ?? '';
                    break;
            }
        }
        return $walletLogs;
    }

    public static function getListByType($wallet_type, $types, $status = 'all')
    {
        $user = User::info();
        //$model = new \app\admin\model\User();
        //$user = $model->find(6);
        $walletLogs = self::{$wallet_type}();

        if ($status != 'all') {
            $walletLogs = $walletLogs->{$status}();
        }
        if(empty($user)){
            $user_id = 0;
        }else{
            $user_id = $user->id;
        }
        $walletLogs = $walletLogs->where(['user_id' => $user_id,'type'=>['in',$types]])
            ->order('id', 'DESC')->paginate(10);
        foreach ($walletLogs as $w) {
            $w->avatar = $user->avatar;
            $w->createtime = date('Y-m-d H:i:s',$w->createtime);
            switch ($w['type']) {
                case 'wallet_pay':
                case 'wallet_refund':
                    $item = OrderItem::get($w->item_id);
                    $w->avatar = $item['goods_image'] ?? '';
                    $w->title = $item['goods_title'] ?? '';
                    break;
                case 'cash':
                case 'cash_error':
                    $userWalletApply = UserWalletApply::get($w->item_id);
                    $apply = ($userWalletApply && $userWalletApply['bank_info']) ? json_decode($userWalletApply['bank_info'], true) : [];
                    // $w->avatar = $user->avatar;
                    $w->title = $apply['bank_name'] ?? '';
                    break;
            }
        }
        return $walletLogs;
    }

    public static function getSumByType($wallet_type, $types, $status = 'all',$user_id= 0)
    {
        if($user_id == 0){

            $user = User::info();
            $user_id = $user->id;
        }
        $walletLogs = self::{$wallet_type}();

        if ($status != 'all') {
            $walletLogs = $walletLogs->{$status}();
        }
        $sum = $walletLogs->where(['user_id' => $user_id,'type'=>['in',$types]])->sum('wallet');

        return $sum;
    }

    public static function getTypeName($type)
    {
        return isset(self::$typeAll[$type]) ? self::$typeAll[$type]['name'] : '';
    }


    public function getTypeNameAttr($value, $data)
    {
        return self::getTypeName($data['type']);
    }


    public function getWalletTypeNameAttr($value, $data)
    {
        return self::$walletTypeAll[$data['wallet_type']] ?? '';
    }


    public function getWalletAttr($value, $data)
    {
        return $data['wallet_type'] == 'score' ? intval($value) : $value;
    }

}
