<?php

return [
    [
        'name' => 'appKey',
        'title' => '百度appKey ',
        'type' => 'string',
        'content' => [],
        'value' => 'cPGGgL99ThQ8sENYiEIHINP0',
        'rule' => 'required',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'secretKey',
        'title' => '百度secretKey',
        'type' => 'string',
        'content' => [],
        'value' => 's4c69n4fD2kgWRV5BDYa1yVBpVKhepNF',
        'rule' => 'required',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'state',
        'title' => '是否开启',
        'type' => 'radio',
        'content' => [
            '否',
            '是',
        ],
        'value' => '1',
        'rule' => 'required',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],
];
